# order-service
This module contains the order service module

![Flamingo image](https://cdn1.vectorstock.com/i/1000x1000/27/05/flamingo-cartoon-vector-892705.jpg)

# Description
A docker file is created to run nodejs
The image has all the file inside and expose PORT 8080
.dockerignore is included to ignore all the dependencies
Dependencies will be installed by the RUN command

# How to build image
Simply run
    docker build -t littleamazon/product-service-node .

# How to run image
Simply run
    docker run -p 8080:8080 -d littleamazon/product-service-node

:fireworks: enjoy :fireworks: