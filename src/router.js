const Router = require('express').Router;
const helloWorldController = require('./controllers/helloWorld').helloWorld;
const orderController = require('./controllers/order_controller');
const orderValidator = require('./validators/order_validator').createOrderValidator;
const orderValidatorCheck = require('./middleware/order_validation_check').handleValidationResult;

//init router
const router = Router();

//test call
router.get('/', helloWorldController);

router.get('/order', orderController.allOrders);
router.post('/order', orderValidator, orderValidatorCheck, orderController.newOrder);
router.delete('/order', orderController.deleteAllOrders);
router.get('/order/:orderID', orderController.getOrder);
router.delete('/order/:orderID', orderController.deleteOrder);
router.get('/order/user/:userID', orderController.findByUserID);

router.get('/order/:orderID/amount/:currency', orderController.getAmount); //external api call

module.exports = router;