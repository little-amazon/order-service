const nano = require('../db');

//create adapter to call external service
const apiAdapter = require('../api_adapter');
const BASE_URL = "http://data.fixer.io/api/latest?access_key=18e9696c52ff3752eabddd7a6dacff20&base=EUR";
const api = apiAdapter(BASE_URL);


async function newOrder(req, res) {
    //get body
    const jsBody = req.body;

    //insert new
    nano.insertOrder(jsBody).then(function(body) {
        res.status(200).send(body);
    
    }, function(err) {
        res.status(400).send(err);
    });
    
}

async function allOrders(req, res) {
    //get all products
    nano.retrieveAllOrders().then(function(body) {
        res.status(200).send(body);
    
    }, function(err) {
        res.status(400).send(err);
    });
}

async function deleteAllOrders(req, res) {
    //first get all prod
    nano.retrieveAllOrders().then(function(body) {
        
        const allRows = body.rows;

        var successCount = 0;
        for(const row of allRows) {
            //then destroy doc
            nano.destroyDoc(row.id, row.value.rev).then(function(body) {
                successCount++;

                //if last element return success
                if(successCount === allRows.length) {
                    res.status(200).send({"message" : "all deleted"});
                }
            
            }, function(err) {
                res.status(400).send(err);
                return; //to exit from for-loop
            });
        }
    
    }, function(err) {
        res.status(400).send(err);
    });
}

async function getOrder(req, res) {
    var oID = req.params.orderID;

    nano.retrieveOrder(oID).then(function(body) {
        res.status(200).send(body);
    
    }, function(err) {
        res.status(400).send(err);
    });
}

async function deleteOrder(req, res) {
    var oID = req.params.orderID;

    //first get prod to retrieve revision
    nano.retrieveOrder(oID).then(function(body) {

        //then destroy doc
        nano.destroyDoc(oID, body._rev).then(function(body) {
            res.status(200).send(body);
        
        }, function(err) {
            res.status(400).send(err);
        });
    
    }, function(err) {
        res.status(400).send(err);
    });
}

async function findByUserID(req, res) {
    const uID = String(req.params.userID);

    //first get prod to retrieve revision
    nano.fetchOrders({  'selector' : {'userID' : uID}}).then(function(body) {

        //then destroy doc
        res.status(200).send(body);
    
    }, function(err) {
        res.status(400).send(err);
    });

}

async function getAmount(req, res) {
    const oID = req.params.orderID;
    const currency = req.params.currency;

    if(currency !== "EUR" && currency !== "USD") {
        res.status(400).send(new Error('Invalid currency'));
    }

    nano.retrieveOrder(oID).then(function(body) {

        const itemList = body.item_list;
        var amountEUR = 0.0;
        
        for(const element of itemList) {
            amountEUR += element.price * element.quantity;
        }

        if(currency === "EUR") {
            //round number
            amountEUR = Number((amountEUR).toFixed(2));
            res.status(200).send({'id' : oID, 'amount' : amountEUR, 'currency' : currency});
        } else {
            //make here a call to api to convert eur to usd
            api.get('').then(resp => {
                const usdRate = resp.data.rates.USD;
                var amountUSD = amountEUR * usdRate; //calculate
                amountUSD = Number((amountUSD).toFixed(2)); //round
                res.status(200).send({'id' : oID, 'amount' : amountUSD, 'currency' : currency});
            });
        }
    
    }, function(err) {
        res.status(400).send(err);
    });

}




module.exports = {
    newOrder: newOrder,
    allOrders: allOrders,
    deleteAllOrders: deleteAllOrders,
    getOrder: getOrder,
    deleteOrder: deleteOrder,
    findByUserID: findByUserID,
    getAmount: getAmount
}