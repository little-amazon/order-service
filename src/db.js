const USERNAME = '';
const PASSWORD = '';
const IP =  process.env.ORDER_DB_URL//'192.168.99.100';
const PORT =  process.env.ORDER_DB_PORT//'5984';
const DB_NAME = 'order_database';

const nano = require('nano')('http://' + IP + ':' + PORT);

//create adapter to call external service
const apiAdapter = require('./api_adapter');
const BASE_URL = "http://product_endpoint:8080/product";
const api = apiAdapter(BASE_URL);

var db = null;

function createConnection() {
    try {
        //create nanodb if not exists
        nano.db.create(DB_NAME, function(err, body, header) {
            if (!err || err.error == 'file_exists') {
                db = nano.use(DB_NAME);
            } else if(err.code === 'ECONNREFUSED') {
                createConnection(); //recursive call
            } else {
                console.log(err);
                process.exit(1);
            }
        });
    } catch(exc) {
        createConnection();
    } 
    
}

//create db connection
createConnection();


//DELETE DOC
async function destroyDoc(dID, dRev) {
    return new Promise(function(resolve, reject) {
        db.destroy(dID, dRev, function(err, body) {
            if(err) {
                reject(err);
            } else {
                resolve(body);
            }
        });
    });
}

//RETRIEVE ALL
async function retrieveAllOrders() {
    return new Promise(function(resolve, reject) {
        db.list({include_docs: true},function(err,body) {
            if(err) {
                reject(err);
            } else {
                resolve(body);
            }
        });
    });
}

//RETRIEVE SINGLE
async function retrieveOrder(pID) {
    return new Promise(function(resolve, reject) {
        db.get(pID, { revs_info: false }, function(err, body) {
            if(err) {
                reject(err);
            } else {
                resolve(body);
            }
        });
    });
}

async function fetchOrders(keys) {
    return new Promise(function(resolve, reject) {
        db.find(keys, function(err, body) {
            if(err) {
                reject(err);
            } else {
                resolve(body);
            }
        });
    });
}

//INSERT NEW
async function insertOrder(jsBody) {
    return new Promise(function(resolve, reject) {
        db.insert(jsBody, {}, function(err, body) {
            if (err) {
                reject(err);
            } else {

                //here update products availability

                //delete quantity async
                console.log(JSON.stringify(jsBody.item_list));
                jsBody.item_list.forEach(element => {
                    
                    const pID = element.idProduct;
                    const quantity = element.quantity;

                    const url = '/' + pID + '/' + quantity;
                        console.log(url);
                        api.delete(url).then(resp => {
                            console.log(resp);
                        }).catch(function (error) {
                            // handle error
                            console.log(error);
                        });
                });

                //at the end return body
                resolve(body);
            }
        });
    });
}


module.exports = {
    destroyDoc : destroyDoc,
    retrieveAllOrders : retrieveAllOrders,
    insertOrder : insertOrder,
    retrieveOrder : retrieveOrder,
    fetchOrders: fetchOrders
};
