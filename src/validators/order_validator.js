const { body } = require('express-validator/check');

module.exports.createOrderValidator = [
  body('userID', 'userID is required').exists(),
  body('item_list', 'list of products is required').exists().isArray()
];
